Modulo para resolver las prueba ACG.
Se recomienda generar contenido con Devel generate, este módulo usa paragraphs para llenar los planes
pero estos contenidos no son generados por devel y deben ser agregados manualmente.

Equipos: tipos
--------------------------------

/admin/structure/taxonomy/mobile_product_types

Equipos: marcas
---------------------------

/admin/structure/taxonomy/mobile_product_brands

Equipos
------------------------------------

Agregar contenido: /node/add/mobile-product
Lista con filtros: /mobile-product
Editar vista: /admin/structure/views/view/mobile_product/edit/page
