<?php

/**
 * @file
 * d7_test_acg.features.inc
 */

/**
 * Implements hook_views_api().
 */
function d7_test_acg_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function d7_test_acg_node_info() {
  $items = array(
    'mobile_product' => array(
      'name' => t('Mobile product'),
      'base' => 'node_content',
      'description' => t('Contains mobile products off all types.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function d7_test_acg_paragraphs_info() {
  $items = array(
    'mobile_product_plan' => array(
      'name' => 'Mobile product: plan',
      'bundle' => 'mobile_product_plan',
      'locked' => '1',
    ),
  );
  return $items;
}
